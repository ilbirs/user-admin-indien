import './App.css';
import React from 'react';
import HomePage from './pages/HomePage'
import {Domains} from './Domains/Domains'
import Users from './Users/Users'
import Rights from './Rights/Rights'
import DoChanging from './Users/DoChanging'
import DoChanging2 from './Domains/DoChanging2'
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {TopMenu} from "./main/TopMenu";

function App() {
    return (
        <BrowserRouter>
            <TopMenu/>
            <div style={{marginTop: '20px', marginBottom: '20px'}}>
                <Switch>
                    <Route path="/SearchDoChanging2">
                        <DoChanging2 />
                    </Route>
                    <Route path="/SearchChanging">
                        <DoChanging/>
                    </Route>
                    <Route path="/SearchRights">
                        <Rights/>
                    </Route>
                    <Route path="/SearchUsers">
                        <Users/>
                    </Route>
                    <Route path="/SearchDomains">
                        <Domains/>
                    </Route>
                    <Route path="/">
                        <HomePage/>
                    </Route>
                </Switch>
            </div>
        </BrowserRouter>
    );
}
export default App;


/*
   <Route path="/SearchChanging">
                        <DoChanging/>
                    </Route>


 <Route path="/SearchRemoveRights">
                       <RemoveRightsFrom/>
                   </Route>*/