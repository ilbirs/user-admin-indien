import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import {makeStyles} from "@material-ui/core/styles";
import {Link} from "react-router-dom";
import HomeIcon from '@material-ui/icons/Home';
import React, {Component} from "react";
import SportsKabaddiIcon from '@material-ui/icons/SportsKabaddi';
import DomainIcon from '@material-ui/icons/Domain';
import GavelIcon from '@material-ui/icons/Gavel';


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        marginBottom: '10px'
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));


export class TopMenu extends Component {


    render() {
        const classes = {useStyles};
        return (
            <AppBar position="static" className={classes.root}>
                <Toolbar>
                    <Button component={Link} to="/">
                        <HomeIcon/>
                    </Button>
                    <Button component={Link} to="/SearchUsers">
                        <SportsKabaddiIcon/>
                        Users
                    </Button>
                    <Button component={Link} to="/SearchDomains">
                        <DomainIcon/>
                        Domains
                    </Button>
                    <Button component={Link} to="/SearchRights">
                        <GavelIcon/>
                        Rights
                    </Button>
                </Toolbar>
            </AppBar>
        );
    }
}