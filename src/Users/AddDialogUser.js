import React, {Component} from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import DialogActions from "@material-ui/core/DialogActions";

export class AddDialogUser extends Component {


    addUserSubmit = (event) => {
        event.preventDefault();
        fetch('https://usersjava20.sensera.se/users',
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    username: event.target.username.value, password: 'änuvårare att knäcka'
                })
            })
            .then(res => res.json())
            .then((result) => {
                    alert('genomförd');
                    console.log(result)
                },
                (error) => {
                    alert('failed')
                }
            )
    }


    render() {
        return (
            <Dialog
                open={this.props.open}
                aria-labelledby="simple-dialog-title"
            >
                <DialogTitle id="simple-dialog-title">Add user</DialogTitle>
                <DialogContent style={{width: '300px'}}>
                    <DialogContentText>
                        Skapa nytt user
                    </DialogContentText>
                    <form onSubmit={this.addUserSubmit}>
                        <TextField
                            required
                            variant="outlined"
                            autoFocus
                            margin="dense"
                            name='username'
                            id="outlined-basic"
                            label="user"
                            type="text"
                        /><br/>
                        <TextField
                            id="outlined-password-input" label="Password"
                            type="password" autoComplete="current-password"
                            variant="outlined" required
                            autoFocus margin="dense"
                            name='password'
                        /> <br/>
                        <Button variant="contained" color="primary" type="submit"
                                onClick={this.props.onClick}>
                            Add User
                        </Button>
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button variant="contained" color="secondary" onClick={this.props.onClick}>
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }
}