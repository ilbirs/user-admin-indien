import React, {useEffect, useState} from "react";
import RemoveRightsUser from "./RemoveRightsUser";
import AddRightsUser from "./AddRightsUser";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import TableContainer from '@material-ui/core/TableContainer';
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";


function DoChanging(props) {

    const [users, setUsers] = useState([]);
    const [removeDialogOpen, setRemoveDialogOpen] = useState(false);
    const [addDialogOpen, setAddDialogOpen] = useState(false);


    let fetchUsers = () => {
        fetch('https://usersjava20.sensera.se/users')
            .then(res => res.json())
            .then(fetchedUsers => setUsers(fetchedUsers));
    };
    useEffect(() => {
        fetchUsers();
    }, []);


    let removeUser = (username, right, domain) => {
        fetch('https://usersjava20.sensera.se/users/removeRightFromUser', {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username: username,
                right: right,
                domain: domain
            })

        }).then(res => res.json())
            .then(data =>
                console.log(data)
            )
            .then((result) => {
                alert('genomförd');
            },)
            .catch((error) => {
                alert('failed')
            })

    }


    let addRight = (username, right, domain) => {
        fetch('https://usersjava20.sensera.se/users/addRightToUser',
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    username: username,
                    right: right,
                    domain: domain
                })
            })
            .then(res => res.json())
            .then(data =>

                console.log(data)
            )
            .then((result) => {
                alert('genomförd');
            },)
            .catch((error) => {
                alert('failed')
            })
    }


    return (
        <Container>
            <TableContainer component={Paper} style={{width: '1200px', height: '500px'}}>
                <RemoveRightsUser
                    open={removeDialogOpen}
                    onClose={() => setRemoveDialogOpen(false)}
                    remItem={(username, right, domain) => {
                        removeUser(username, right, domain);
                        setRemoveDialogOpen(false);
                    }}
                />
                <AddRightsUser
                    open={addDialogOpen}
                    onClose={() => setAddDialogOpen(false)}
                    addItem={(username, right, domain) => {
                        addRight(username, right, domain);
                        setAddDialogOpen(false);
                    }}
                />
                <Table aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>userName</TableCell>
                            <TableCell align="center">ID</TableCell>
                            <TableCell align="right">Domain/Rights</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {users.map((val) => {
                            return (
                                <TableRow key={val.id}>
                                    <TableCell>
                                        {val.username}
                                    </TableCell>
                                    <TableCell>
                                        {val.id}
                                    </TableCell>
                                    {val.rights.map((rights) => (
                                        <TableCell key={rights.toString()}>
                                            {rights}
                                        </TableCell>
                                    ))}
                                </TableRow>
                            )
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
            <Button variant="contained" color="secondary" type="submit" style={{marginTop: '20px'}}
                    onClick={() => setRemoveDialogOpen(true,)}>
                Remove.Right
            </Button>

            <Button variant="contained" color="primary" type="submit" style={{marginTop: '20px', marginLeft: '20px'}}
                    onClick={() => setAddDialogOpen(true,)}>
                Add.Right
            </Button>
        </Container>
    );
}

export default DoChanging;


/*

 /* {this.setState({  fff:rights.split('.')})}

 <Container>
                <EditDialogUser
                    open={this.state.userDialogShow}
                    username={this.state.username}
                    id={this.state.id}
                    password={this.state.password}
                    Close={DialogClose}/>

                        <RemoveRightsUser
                       open={this.state.userDialogDelete}
                       username={this.state.username}
                       rights={this.state.rights}
                       //domain={this.state.domain}
                       Close2={DialogCloseRemove}/>

 {console.log(rights.split('.'))}


-----------------------------------------------------------------------------------------------------------------*/


