import React, {Component} from "react";
import {EditDialogUser} from "./EditDialogUser";
import {AddDialogUser} from "./AddDialogUser";
import Container from "@material-ui/core/Container";
import SearchBox from "./SearchBox";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import EditIcon from '@material-ui/icons/Edit';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import IconButton from '@material-ui/core/IconButton';
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import {Link} from "react-router-dom";


function searchingFor(searchUser) {
    return function (val) {
        return val.username.toLowerCase().includes(searchUser.toLowerCase()) || !searchUser;
    }
}


class Users extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
            userAddShow: false,
            userDialogShow: false,
            userDialogDelete: false,
            searchUser: '',
        }
    }


    componentDidMount = () => {
        this.refreshList()
    }


    refreshList = () => {
        fetch('https://usersjava20.sensera.se/users')
            .then(response => response.json())
            .then(date => {
                this.setState({users: date});
            })
    }


    searchHandler = (event, searchUser) => {
        event.preventDefault();
        fetch('https://usersjava20.sensera.se/users/find?username=' + searchUser)
            .then(response => response.json())
            .then(date => {
                this.setState({username: date});
            })
        console.log(event.target.value);
        this.setState({searchUser: event.target.value})
    }


    deleteUser(id) {
        if (window.confirm('är du säkert?')) {
            fetch('https://usersjava20.sensera.se/users/' + id, {
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            }).then(r => {
                alert('genomförd');
                console.log(r)
            })
                .catch((error) => {
                    alert('failed')
                })
        }
    }


    render() {
        const {users} = this.state

        let DialogClose = () => {this.setState({userDialogShow: false})}
        let addDialogClose = () => {this.setState({userAddShow: false})}

        return (
            <Container>
                <TableContainer component={Paper} style={{width: '1200px', height: '500px'}}>
                    <AddDialogUser
                        open={this.state.userAddShow}
                        onClick={addDialogClose}/>
                    <EditDialogUser
                        open={this.state.userDialogShow}
                        username={this.state.username}
                        id={this.state.id}
                        password={this.state.password}
                        Close={DialogClose}/>
                    <SearchBox searchHandler={this.searchHandler}/>
                    <Table aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>userName</TableCell>
                                <TableCell align="center">ID</TableCell>
                                <TableCell align="right">Domain/Rights</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {users.filter(searchingFor(this.state.searchUser)).map((val) => {
                                return (
                                    <TableRow key={val.id}>
                                        <TableCell>
                                            {val.username}
                                            <IconButton size="small" variant="contained" color="primary"
                                                        onClick={() => this.setState({
                                                            userDialogShow: true, id: val.id,
                                                            username: val.username})}>
                                                <EditIcon/>
                                            </IconButton>
                                        </TableCell>
                                        <TableCell>
                                            {val.id}
                                            <IconButton size="small" variant="contained" color="inherit"
                                                        onClick={() => this.deleteUser(val.id)}>
                                                <DeleteForeverIcon/>
                                            </IconButton>
                                        </TableCell>
                                        {val.rights.map((rights) => (
                                            <TableCell key={rights.toString()}>
                                                {rights}
                                            </TableCell>
                                        ))}
                                    </TableRow>
                                )
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
                <Button variant="contained" color="primary" type="submit" style={{marginTop: '20px'}}
                        onClick={() => this.setState({userAddShow: true,})}>
                    add user
                </Button>
                <Button variant="contained" color="secondary" component={Link}
                        to="/SearchChanging" style={{marginLeft: '30px', marginTop: '20px'}}>
                    Remove/Add.Rights
                </Button>
            </Container>
        );
    }
}

export default Users;


/*


import React, {Component} from "react";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
//import {makeStyles} from '@material-ui/core/styles';
import Button from "@material-ui/core/Button";
import SearchBox from "./SearchBox";
import {AddDialogUser} from "./AddDialogUser";
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import List from "@material-ui/core/List";
import {Link} from "react-router-dom";



function searchingFor(searchUser) {
    return function (val) {
        return val.username.toLowerCase().includes(searchUser.toLowerCase()) || !searchUser;
    }
}

export class Users extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
            userAddShow: false,
            searchUser: '',

        }
    }

    componentDidMount = () => {
        this.refreshList();
    }

    refreshList = () => {
        fetch('https://usersjava20.sensera.se/users')
            .then(response => response.json())
            .then(date => {
                this.setState({users: date});
            })

    }


    searchHandler = (event, searchUser) => {
        event.preventDefault();
        fetch('https://usersjava20.sensera.se/users/find?username=' + searchUser)
            .then(response => response.json())
            .then(date => {
                this.setState({username: date});
            })
        console.log(event.target.value);
        this.setState({searchUser: event.target.value})
    }


    render() {
        const {users} = this.state
        let addDialogClose = () => this.setState({userAddShow: false});


        return (
            <Container>
                <AddDialogUser
                    open={this.state.userAddShow}
                    onClick={addDialogClose}

                />
                <SearchBox searchHandler={this.searchHandler}/>

                <Paper elevation={1} component="form">
                    <ListItem style={{textAlign: 'center', marginBottom: '10px', border: '2px solid Tomato '}}>
                        <ListItemText primary="UserName"/>
                        <ListItemText primary="ID"/>
                        <ListItemText primary=""/>
                    </ListItem>
                    <List component="nav" aria-label="main mailbox folders" style={{
                        marginBottom: '10px',
                        border: '2px solid DodgerBlue ',
                        overflow: 'auto',
                        maxHeight: 350,
                    }}>

                        {users.filter(searchingFor(this.state.searchUser)).map((val) =>
                            <ListItem key={val.id} style={{borderBottom: '2px solid green'}}>
                                <ListItemText primary={val.username}/>
                                <ListItemText primary={val.id}/>
                            </ListItem>
                        )}
                    </List>

                </Paper>
                <Button variant="contained" color="primary" type="submit"
                        onClick={() => this.setState({userAddShow: true})}>
                    Add User
                </Button>
                <Button variant="contained" color="secondary" component={Link} to="/SearchChanging" style={{marginLeft:'30px'}}>
                    Do.Changing
                </Button>
            </Container>

        );
    }
}



*/

