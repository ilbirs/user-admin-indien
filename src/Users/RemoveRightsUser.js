import React, {useState} from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import TextField from "@material-ui/core/TextField";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";

export default function RemoveRightsUser(props) {

    const [username, setUserName] = useState('');
    const [right, setRight] = useState('');
    const [domain, setDomain] = useState('');


    return (
        <Dialog
            open={props.open}
            onClose={() => props.onClose()}
            aria-labelledby="simple-dialog-title"
        >
            <DialogTitle id="simple-dialog-title">Remove Right</DialogTitle>
            <DialogContent style={{width: '300px'}}>
                <TextField required
                           name='username'
                           autoFocus
                           margin="dense"
                           label="username"
                           type="text"
                           value={username}
                           onChange={(e) => setUserName(e.target.value)}
                /><br/>
                <TextField required
                           name='right'
                           autoFocus
                           margin="dense"
                           label="right"
                           type="text"
                           value={right}
                           onChange={(e) => setRight(e.target.value)}
                /><br/>
                <TextField required
                           autoFocus
                           name='domain'
                           margin="dense"
                           label="Domain"
                           type="text"
                           value={domain}
                           onChange={(e) => setDomain(e.target.value)}
                />
            </DialogContent>
            <DialogActions>
                <Button variant="contained" color="primary"
                        style={{float: 'right'}} onClick={() => {props.remItem(username, right, domain);}}>
                    Update
                </Button>

                <Button variant="contained" color="secondary" onClick={() => props.onClose()}>
                    Cancel
                </Button>
            </DialogActions>
        </Dialog>
    )
}


/*import React, {Component} from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import DialogActions from "@material-ui/core/DialogActions";


export class RemoveRightsUser extends Component {
    constructor(props) {
        super(props);
    }





    deleteRightsFromUser( username, right, domain ) {

        fetch('https://usersjava20.sensera.se/users/removeRightFromUser', {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                user:username,
                right:right,
                domain:domain
            })

        }).then(data =>

            console.log(data)
        )

    }


    handleChangeUser = () => {
        this.setState({username: this.username});
    }
    handleChangeRight = () => {
        this.setState.rights({right: this.right });
    }
    handleChangeDomain = () => {
        this.setState.rights({domain: this.domain });
    }

    render() {
        return(
            <Dialog open={this.props.open} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">edit User</DialogTitle>
                <DialogContent style={{width: '500px'}}>
                    <DialogContentText>
                        Skapa ny namn
                    </DialogContentText>

                    <form onSubmit={this.deleteRightsFromUser}>
                        <TextField
                             label="username"
                            variant="outlined" required
                            autoFocus margin="dense"
                            name='username'  type="text"
                            value={this.props.username}
                             onChange={this.handleChangeUser}
                           fullWidth
                        />
                        <br/>
                        <TextField
                             variant="outlined"
                            required autoFocus
                            margin="dense" name='right'
                            label="right" type="text"
                            value={this.props.right}
                             onChange={this.handleChangeRight}
                        />
                        <br/>
                        <TextField
                            variant="outlined"
                            required autoFocus
                            margin="dense" name='domain'
                            label="domain" type="text"
                            dalue={this.props.domain}
                            onChange={this.handleChangeDomain}
                        />
                        <Button variant="contained" color="primary" type="submit" onClick={this.props.Close2}>
                            update User
                        </Button>

                    </form>
                </DialogContent>
                <DialogActions>
                    <Button variant="contained" color="secondary" onClick={this.props.Close2}>
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }




}*/