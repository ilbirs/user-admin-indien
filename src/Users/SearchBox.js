import React from "react";
import Box from "@material-ui/core/Box";
import Paper from "@material-ui/core/Paper";
import IconButton from "@material-ui/core/IconButton";
import SearchIcon from "@material-ui/icons/Search";
import InputBase from "@material-ui/core/InputBase";


function SearchBox(props) {


    return (
        <Box style={{marginBottom: '10px', display: 'flow-root'}} >
            <span>USERS</span>
            <Paper component="form" style={{float: 'right', width: '500px' }}>
                <IconButton type="submit" aria-label="search">
                    <SearchIcon/>
                </IconButton>
                <InputBase
                    placeholder="Sök efter namn"
                    inputProps={{'aria-label': 'Sök namn'}}
                    onChange={props.searchHandler}
                    type="text"
                />
            </Paper>
        </Box>
    )
}

export default SearchBox;