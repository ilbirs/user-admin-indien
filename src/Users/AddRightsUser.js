import React, {useState} from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import TextField from "@material-ui/core/TextField";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";


export default function AddRightsUser(props) {

    const [username, setUserName] = useState('');
    const [right, setRight] = useState('');
    const [domain, setDomain] = useState('');


    return (
        <Dialog
            open={props.open}
            onClose={() => props.onClose()}
            aria-labelledby="simple-dialog-title"
        >
            <DialogTitle id="simple-dialog-title">Add Right</DialogTitle>
            <DialogContent style={{width: '300px'}}>
                <TextField required
                           name='username'
                           autoFocus
                           margin="dense"
                           label="username"
                           type="text"
                           value={username}
                           onChange={(e) => setUserName(e.target.value)}
                           fullWidth
                /><br/>
                <TextField required
                           name='right'
                           autoFocus
                           margin="dense"
                           label="right"
                           type="text"
                           value={right}
                           onChange={(e) => setRight(e.target.value)}
                           fullWidth
                /><br/>
                <TextField required
                           autoFocus
                           name='domain'
                           margin="dense"
                           label="Domain"
                           type="text"
                           value={domain}
                           onChange={(e) => setDomain(e.target.value)}
                           fullWidth
                /><br/>
            </DialogContent>
            <DialogActions>
                <Button variant="contained" color="primary" style={{float: 'right'}} onClick={() => {
                    props.addItem(username, right, domain);
                }}>
                    Update
                </Button>
                <Button variant="contained" color="secondary" onClick={() => props.onClose()}>
                    Cancel
                </Button>
            </DialogActions>
        </Dialog>
    )
}
