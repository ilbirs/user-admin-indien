import React, {Component} from "react";
import '../App.css';
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import List from "@material-ui/core/List";
import {AddRights} from './AddRights'
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {withStyles} from '@material-ui/core/styles';


const styles = ((theme) => ({
    root: {
        flexGrow: 1,
        maxWidth: 752,
    },
    demo: {
        backgroundColor: theme.palette.background.paper,
    },
    title: {
        margin: theme.spacing(4, 0, 2),
    },
}));


class Rights extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rights: [],
            addShow: false,
        }
    }


    componentDidMount = () => {
        this.refreshList();
    }


    refreshList = () => {
        fetch('https://usersjava20.sensera.se/rights')
            .then(response => response.json())
            .then(date => {
                this.setState({rights: date});
            })
    }


    render() {
        const {classes} = this.props;
        const {rights} = this.state;
        let DialogClose = () => this.setState({addShow: false});
        return (
            <Container>
                <h3>RIGHTS</h3>
                <AddRights
                    open={this.state.addShow}
                    key={this.state.key}
                    domain={this.state.domain}
                    Close={DialogClose}/>
                <Paper style={{border: '2px solid Tomato', marginBottom: '10px'}}>
                    <Grid container spacing={1}>
                        <Grid item xs={8} md={2}>
                            <Typography variant="h6" className={classes.title} style={{marginLeft: '50px'}}>
                                Rights
                            </Typography>
                        </Grid>
                        <Grid item xs={9} md={4}>
                            <Typography variant="h6" className={classes.title} style={{marginLeft: '50px'}}>
                                RightsID
                            </Typography>
                        </Grid>
                        <Grid item xs={8} md={3}>
                            <Typography variant="h6" className={classes.title} style={{marginLeft: '50px'}}>
                                Domain
                            </Typography>
                        </Grid>
                    </Grid>
                </Paper>
                <Paper component="nav" aria-label="main mailbox folders" style={{
                    marginBottom: '10px',
                    border: '2px solid DodgerBlue ',
                    overflow: 'auto',
                    maxHeight: 350,
                }}>
                    <Grid container spacing={1}>
                        <Grid item xs={8} md={4}>
                            <List>
                                {rights.map((val) =>
                                    <ListItem key={val.id} style={{borderBottom: '2px solid green', height: '85px'}}>
                                        <ListItemText primary={val.key}/>
                                    </ListItem>
                                )}
                            </List>
                        </Grid>
                        <Grid item xs={9} md={4}>
                            <List>
                                {rights.map((val) =>
                                    <ListItem key={val.id} style={{borderBottom: '2px solid green', height: '85px'}}>
                                        <ListItemText
                                            primary={val.id}/>
                                    </ListItem>,
                                )}
                            </List>
                        </Grid>
                        <Grid item xs={8} md={4}>
                            <List>
                                {rights.map((val) =>
                                    <ListItem key={val.id} style={{borderBottom: '2px solid green', height: '85px'}}>

                                        <ListItemText
                                            primary={val.domain}
                                        />
                                    </ListItem>
                                )}
                            </List>
                        </Grid>
                    </Grid>
                </Paper>
                <Button variant="contained" color="primary" onClick={() => this.setState({addShow: true})}>
                    Add Rights
                </Button>
            </Container>
        );
    }
}

export default withStyles(styles, {withTheme: true})(Rights);










/*     <Container>

                <AddRights
                    open={this.state.addShow}
                    key={this.state.key}
                    domain={this.state.domain}
                    Close={DialogClose}/>

                <Paper elevation={1} component="form">
                    <ListItem style={{textAlign: 'center', marginBottom: '10px', border: '2px solid Tomato '}}>
                        <ListItemText primary="KEY"/>
                        <ListItemText primary="ID"/>
                        <ListItemText primary="Domain"/>
                        <ListItemText primary=""/>
                    </ListItem>
                    <List component="nav" aria-label="main mailbox folders" style={{
                        marginBottom: '10px',
                        border: '2px solid DodgerBlue ',
                        overflow: 'auto',
                        maxHeight: 350,
                    }}>

                        {rights.map((val) =>
                            <ListItem key={val.id} style={{borderBottom: '2px solid green'}}>
                                <ListItemText primary={val.key}/>
                                <ListItemText primary={val.id}/>
                                <ListItemText primary={val.domain}/>
                            </ListItem>
                        )}
                    </List>
                </Paper>

                <Button variant="contained" color="primary" onClick={() => this.setState({addShow: true})}>
                    Add Rights
                </Button>
            </Container>*/

