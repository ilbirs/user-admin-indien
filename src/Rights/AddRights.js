import React, {Component} from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import DialogActions from "@material-ui/core/DialogActions";
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText'
import List from "@material-ui/core/List";
import Typography from "@material-ui/core/Typography";


export class AddRights extends Component {
    constructor(props) {
        super(props);
        this.state = {
            domains: [],
        }
    }


    componentDidMount = () => {
        this.refreshList2();
    }


    refreshList2 = () => {
        fetch('https://usersjava20.sensera.se/domains')
            .then(response => response.json())
            .then(date => {
                this.setState({domains: date});
            })
    }


    addRightsSubmit = (event) => {
        event.preventDefault();
        /* const newDomain = this.state.domain;*/
        fetch('https://usersjava20.sensera.se/rights',
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    key: event.target.key.value,
                    domain: event.target.domain.value,
                })
            })
            .then(response => {
                return response.json()
            }).then((result) => {
                alert('genomförd');
                console.log(result)
            },
            (error) => {
                alert('failed')
            }
        )
    }


    handleChange = (event) => {
        this.setState({key: event.target.value});
    }
    handleChange2 = (event) => {
        this.setState({domain: event.target.value});
    }


    render() {
        const {domains} = this.state;
        return (
            <Dialog open={this.props.open} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Add Rights</DialogTitle>
                <DialogContent style={{width: '500px'}}>
                    <DialogContentText>
                        Skapa nytt right
                    </DialogContentText>
                    <form onSubmit={this.addRightsSubmit}>
                        <TextField
                            id="outlined-basic"
                            variant="outlined"
                            required
                            autoFocus
                            margin="dense"
                            name='key'
                            label="rights"
                            type="text"
                            value={this.props.key}
                            onChange={this.handleChange}
                        /><br/>
                        <TextField
                            id="outlined-basic"
                            variant="outlined"
                            required
                            autoFocus
                            margin="dense"
                            name='domain'
                            label="domain"
                            type="text"
                            value={this.props.domain}
                            onChange={this.handleChange2}
                        />
                        <List style={{
                            marginBottom: '10px',
                            border: '2px solid DodgerBlue ',
                            overflow: 'auto',
                            maxHeight: 350,
                            height: '100px', width: '218px'
                        }}>
                            {domains.map((val) =>
                                <ListItem key={val.id} value={val.domain}>
                                    <ListItemText primary={val.domain}/>
                                </ListItem>)}
                        </List>
                        <Typography variant="h6">
                            Domains List Alternativ
                        </Typography><br/>
                        <Button variant="contained" color="primary" type="submit" style={{marginTop: '10px'}}
                                onClick={this.props.Close}>
                            Update
                        </Button>
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button variant="contained" color="secondary" onClick={this.props.Close}>
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }
}

/*


     .then(res => res.json())
                .then((result) => {
                        alert('genomförd');
                    },
                    (error) => {
                        alert('failed')
                    }
                )*

  <TextField

                            id="outlined-basic"
                            variant="outlined"
                            required
                            autoFocus
                            margin="dense"
                            name='domain'
                            label="domain"
                            type="text"
                            //onChange={this.handleChange}
                            value={this.props.domain}
                        /><br/>*/