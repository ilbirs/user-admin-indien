import React, {Component} from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import DialogActions from "@material-ui/core/DialogActions";


export class EditDialogDomain extends Component {


    editDomainSubmit(event) {
        event.preventDefault();
        fetch('https://usersjava20.sensera.se/domains/' + event.target.id.value,
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    id: event.target.id.value,
                    domain: event.target.domain.value
                })
            })
            .then(response => {
                return response.json()
            }).then((result) => {
                alert('genomförd');
                console.log(result)
            },
            (error) => {
                alert('failed')
            }
        )
    }


    render() {
        return (
            <Dialog open={this.props.open} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">edit Domain</DialogTitle>
                <DialogContent style={{width: '500px'}}>
                    <DialogContentText>
                        Skapa ny namn
                    </DialogContentText>
                    <form onSubmit={this.editDomainSubmit}>
                        <TextField
                            id="filled-read-only-input" label="ID"
                            variant="outlined" required
                            autoFocus margin="dense"
                            name='id' disabled type="text"
                            defaultValue={this.props.domid}
                            fullWidth
                        />
                        <br/>
                        <TextField
                            id="outlined-basic" variant="outlined"
                            required autoFocus
                            margin="dense" name='domain'
                            label="domain" type="text"
                            defaultValue={this.props.domain}
                        />
                        <br/>
                        <Button variant="contained" color="primary" type="submit" onClick={this.props.editClose}>
                            update User
                        </Button>
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button variant="contained" color="secondary" onClick={this.props.editClose}>
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }
}