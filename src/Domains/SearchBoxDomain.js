import React from "react";
import Box from "@material-ui/core/Box";
import Paper from "@material-ui/core/Paper";
import IconButton from "@material-ui/core/IconButton";
import SearchIcon from "@material-ui/icons/Search";
import InputBase from "@material-ui/core/InputBase";


function SearchBoxDomain(props) {


    return (
        <Box style={{marginBottom: '10px', display: 'flow-root'}}>
            <span>Domain</span>
            <Paper component="form" style={{float: 'right', width: '500px'}}>
                <IconButton type="submit" aria-label="search">
                    <SearchIcon/>
                </IconButton>
                <InputBase
                    name='domain'
                    id="DomainName"
                    label="domain"
                    placeholder="Sök efter namn"
                    inputProps={{'aria-label': 'Sök namn'}}
                    onChange={props.searchHandlerDom}
                    type="text"
                />
            </Paper>
        </Box>
    )
}

export default SearchBoxDomain;