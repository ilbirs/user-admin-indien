import React, {Component} from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import DialogActions from "@material-ui/core/DialogActions";


export class AddDialogDomain extends Component {


    addDomainSubmit = (event) => {
        event.preventDefault();
        fetch('https://usersjava20.sensera.se/domains',
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    domain: event.target.domain.value
                })
            })

            .then(res => res.json())
            .then((result) => {
                    alert('genomförd');
                    console.log(result)
                },
                (error) => {
                    alert('failed')
                }
            )
    }


    render() {

        return (
            <Dialog
                open={this.props.open}
                aria-labelledby="simple-dialog-title"
            >
                <DialogTitle id="simple-dialog-title">Add domain</DialogTitle>
                <DialogContent style={{width: '500px'}}>
                    <form onSubmit={this.addDomainSubmit}>
                        <TextField
                            required
                            autoFocus
                            margin="dense"
                            name='domain'
                            id="domain"
                            label="domain"
                            type="text"
                            // value={this.state.username}
                        />
                        <br/>
                        <Button variant="contained" color="primary" type="submit"
                                onClick={this.props.onClick}>
                            Update
                        </Button>
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button variant="contained" color="secondary" onClick={this.props.onClick}>
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog>

        )
    }
}