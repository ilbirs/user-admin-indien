import React, {Component} from "react";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import List from "@material-ui/core/List";
import {Link} from "react-router-dom";
import SearchBoxDomain from "./SearchBoxDomain";
import {AddDialogDomain} from "./AddDialogDomain";


function searchingFor(searchDomain) {
    return function (val) {
        return val.domain.toLowerCase().includes(searchDomain.toLowerCase()) || !searchDomain;
    }
}


export class Domains extends Component {
    constructor(props) {
        super(props);
        this.state = {
            domains: [],
            domainDialogAdd: false,
            searchDomain: '',
        }
    }


    componentDidMount = () => {
        this.refreshList();
    }


    refreshList = () => {
        fetch('https://usersjava20.sensera.se/domains')
            .then(response => response.json())
            .then(date => {
                this.setState({domains: date});
            })
    }


    searchHandlerDom = (event, searchDomain) => {
        event.preventDefault();
        fetch('https://usersjava20.sensera.se/domains/find?domain=' + searchDomain)
            .then(response => response.json())
            .then(date => {
                this.setState({domain: date});
            })
        console.log(event.target.value);
        this.setState({searchDomain: event.target.value})
    }


    render() {
        const {domains} = this.state;
        let addDialogClose = () => this.setState({domainDialogAdd: false});

        return (
            <Container>
                <AddDialogDomain
                    open={this.state.domainDialogAdd}
                    onClick={addDialogClose}
                />
                <SearchBoxDomain searchHandlerDom={this.searchHandlerDom}/>

                <Paper elevation={1} component="form">
                    <ListItem style={{textAlign: 'center', marginBottom: '10px', border: '2px solid Tomato '}}>
                        <ListItemText primary="Domain"/>
                        <ListItemText primary="ID"/>
                        <ListItemText primary=""/>
                    </ListItem>
                    <List component="nav" aria-label="main mailbox folders" style={{
                        marginBottom: '10px',
                        border: '2px solid DodgerBlue ',
                        overflow: 'auto',
                        maxHeight: 350,
                    }}>
                        {domains.filter(searchingFor(this.state.searchDomain)).map((val) =>
                            <ListItem key={val.id} style={{borderBottom: '2px solid green'}}>
                                <ListItemText primary={val.domain}/>
                                <ListItemText primary={val.id}/>
                            </ListItem>
                        )}
                    </List>
                </Paper>
                <Button variant="contained" color="primary" onClick={() => this.setState({domainDialogAdd: true})}>
                    Add Domain
                </Button>
                <Button variant="contained" color="secondary" component={Link} to="/SearchDoChanging2"
                        style={{marginLeft: '30px'}}>
                    Do.Changing
                </Button>
            </Container>
        );
    }
}


