import React, {Component} from "react";
import {EditDialogDomain} from "./EditDialogDomain";
import SearchBoxId from "./SearchBoxId";
import Paper from "@material-ui/core/Paper";
import EditIcon from '@material-ui/icons/Edit';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import IconButton from '@material-ui/core/IconButton';
import Container from "@material-ui/core/Container";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import DeleteSweepIcon from '@material-ui/icons/DeleteSweep';


function searchingFor(searchDomain) {
    return function (val) {
        return val.id.toLowerCase().includes(searchDomain.toLowerCase()) || !searchDomain;
    }
}


class DoChanging2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            domains: [],
            editDialogShow: false,
            removeRightsShow: false,
            searchDomain: '',
        }
    }


    componentDidMount = () => {
        this.refreshList();
    }


    refreshList = () => {
        fetch('https://usersjava20.sensera.se/domains')
            .then(response => response.json())
            .then(date => {
                this.setState({domains: date});
            })
    }


    searchHandlerDomId = (event, searchDomain) => {
        event.preventDefault();
        fetch('https://usersjava20.sensera.se/domains/find?domain=' + searchDomain)
            .then(response => response.json())
            .then(date => {
                this.setState({id: date});
            })
        console.log(event.target.value);
        this.setState({searchDomain: event.target.value})
    }


    deleteDomain(id) {
        if (window.confirm('är du säkert?')) {
            fetch('https://usersjava20.sensera.se/domains/' + id, {
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
            }).then(r => {
                if (r.ok) {
                    alert('genomförd')
                } else {
                    alert('WARNING!!! you need to remove right from domain')
                }
                console.log(r)
            })
                .catch((error) => {
                    alert('failed')
                })

        }
    }

    deleteRightsFromDomain(id, rightName) {
        fetch('https://usersjava20.sensera.se/domains/' + id + '/' + rightName, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: id,
                rightName: rightName
            })
        })
            .then(r => {
                if (r.ok) {
                    alert('genomförd')
                } else {
                    alert('WARNING!!! you need to remove right from user')
                }
                console.log(r)
            })
    }


    render() {
        const {domains} = this.state
        let EditDialogClose = () => {
            this.setState({editDialogShow: false});
        }

        return (
            <Container>
                <TableContainer component={Paper} style={{width: '1200px', height: '500px'}}>
                    <EditDialogDomain
                        open={this.state.editDialogShow}
                        domain={this.state.domain}
                        domid={this.state.id}
                        editClose={EditDialogClose}/>

                    <SearchBoxId searchHandlerDomId={this.searchHandlerDomId}/>

                    <Table aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Domain</TableCell>
                                <TableCell align="center">DomainID</TableCell>
                                <TableCell align="right">Rights</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {domains.filter(searchingFor(this.state.searchDomain)).map((val) => {
                                return (
                                    <TableRow key={val.id}>
                                        <TableCell>
                                            {val.domain}
                                            <IconButton size="small" variant="contained" color="primary"
                                                        onClick={() => this.setState({
                                                            editDialogShow: true, id: val.id,
                                                            domain: val.domain
                                                        })}>
                                                <EditIcon/>
                                            </IconButton>
                                        </TableCell>
                                        <TableCell>
                                            {val.id}
                                            <IconButton size="small" variant="contained" color="inherit"
                                                        onClick={() => this.deleteDomain(val.id)}>
                                                <DeleteForeverIcon/>
                                            </IconButton>
                                        </TableCell>
                                        {val.rights.map((rights) => (
                                            <TableCell key={rights.toString()}>
                                                {rights}
                                                <IconButton size="small" variant="contained" color="secondary"
                                                            onClick={() => this.deleteRightsFromDomain(val.id, rights)}>
                                                    <DeleteSweepIcon/>
                                                </IconButton>
                                            </TableCell>
                                        ))}
                                    </TableRow>
                                )
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Container>
        );
    }
}

export default DoChanging2;


/*   <RemoveRights
                    open={this.state.removeRightsShow}
                    domid={this.state.id}
                    rights={this.state.rights}
                    removeClose={RemoveDialogClose}/>

                     <Button variant="contained" color="secondary" component={Link} to="/SearchRemoveRights"
                        style={{marginLeft: '30px'}}>
                    remove Rights
                </Button>



                    <DialogActions style={{marginTop: '40px'}}>
                                            <IconButton size="small" variant="contained" color="primary"
                                                        style={{marginTop: '-70px', float: 'right',}}
                                                        onClick={() => this.setState({
                                                            editDialogShow: true, id: val.id,
                                                            domain: val.domain
                                                        })}
                                            >
                                                <EditIcon/>
                                            </IconButton>
                                        </DialogActions>
                                        <DialogActions style={{marginTop: '40px'}}>
                                            <IconButton size="small" variant="contained" color="secondary"
                                                        style={{marginTop: '-70px', float: 'right',}}
                                                        onClick={() => this.deleteDomain(val.id)}
                                            >
                                                <DeleteForeverIcon/>
                                            </IconButton>
                                        </DialogActions>
                                        <DialogActions style={{marginTop: '40px'}}>
                                            <Button size="small" variant="contained" color="secondary"
                                                    style={{marginTop: '-70px', float: 'right', width: '84px'}}
                                                    onClick={() => this.deleteRightsFromDomain(val.id, val.rights)}

                                                    onClick={() => this.setState
                                                    ({removeRightsShow: true, id:val.id, rights:val.rights})} >
                                                <DeleteForeverIcon/>
                                                Rights
                                            </Button>
                                        </DialogActions>


  <Grid item xs={8} md={2}>


                            <List>
                                {domains.filter(searchingFor(this.state.searchDomain)).map((val) =>
                                    <ListItem key={val.id} style={{borderBottom: '2px solid green', height: '85px'}}>
                                        <ListItemText primary={val.domain}/>
                                    </ListItem>
                                )}
                            </List>
                        </Grid>




<Button variant="contained" color="secondary" component={Link} to="/SearchRemoveRights"
                        style={{marginLeft: '30px'}}>
                    del right
                </Button>
                    */